;;; synchro-typing-mode.el --- synchro typing mode -*- lexical-binding: t -*-

;; Copyright (C) 2019-2019  Philippe Brochard

;; Author: Philippe Brochard <hocwp@free.fr>
;; URL: https://bitbucket.org/pbrhocwp/tag-buffer-name
;; Version: 0.0.1
;; Package-Requires: ((emacs "24.1"))
;; Keywords: matching

;; This file is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3, or (at your option)
;; any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; For a full copy of the GNU General Public License
;; see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; This package provides `synchro-typing-mode' functions to functions to
;; synchronize typing over every visible windows.
;;
;; Use synchro-typing-mode function to start/stop the synchronized typing
;; over every visible windows.

;;; Code:

(defgroup synchro-typing nil
  "Synchronize typing over every visible windows."
  :group 'tools
  :link '(url-link :tag "Website"
		  "https://bitbucket.org/pbrhocwp/synchro-typing-mode"))

(defcustom synchro-typing-stop-keys '("M-x" "C-g")
  "List of keys which stop the synchronized typing."
  :group 'synchro-typing
  :type '(repeat (string)))

(defcustom synchro-typing-ignored-keys
  '("M-y" "M-w" "C-w"
	"C-x o"
	"<S-right>" "<S-left>" "<S-up>" "<S-down>")
  "List of keys which are ignored by the synchronized typing."
  :group 'synchro-typing
  :type '(repeat (string)))

(defun synchro-typing-hook ()
  "Hook used to synchronize typing over every visible windows."
  (unless (active-minibuffer-window)
	(when (this-command-keys)
	  (let ((command (if (eql this-command 'self-insert-command)
						 this-command
					   (key-description (this-command-keys)))))
		(if (member command synchro-typing-stop-keys)
			(synchro-typing-mode -1)
		  (unless (member command synchro-typing-ignored-keys)
			(other-window 1)
			(dotimes (_ (1- (length (window-list))))
			  (let* ((cmd (if (functionp command)
							  command
							(key-binding (kbd command))))
					 (arglist (help-function-arglist cmd)))
				(condition-case nil
					(if (and (plusp (length arglist))
							 (not (eq (car arglist) '&optional)))
						(call-interactively cmd 1)
					  (call-interactively cmd))
				  (error nil)))
			  (other-window 1))))))))

(defun synchro-typing-start ()
  "Start to synchro typing over other windows."
  (add-hook 'pre-command-hook 'synchro-typing-hook)
  (message "Synchro typing started"))

(defun synchro-typing-stop ()
  "Stop to synchro typing over other windows."
  (remove-hook 'pre-command-hook 'synchro-typing-hook)
  (message "Synchro typing stopped"))

;;;###autoload
(define-minor-mode synchro-typing-mode
	"synchronize typing over every visible windows."
  :global t
  :lighter " SYNCHRO-TYPING"
  :group 'synchro-typing
  :init-value nil
  (if synchro-typing-mode
	  (synchro-typing-start)
	(synchro-typing-stop))
  (redraw-frame))

(provide 'synchro-typing-mode)

;;; synchro-typing-mode.el ends here
