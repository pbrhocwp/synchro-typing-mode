# synchro-typing-mode

***Synchronize typing over every visible windows***

This package provide **`synchro-typing-mode`** functions to synchronize typing over every visible windows.

***Installation***

Add synchro-typing-mode directory in your load path and require it:

```lisp
(add-to-list 'load-path "/path/to/synchro-typing-mode")
(require 'synchro-typing-mode)
```

***Usage***

Use **`synchro-typing-mode`** function to start/stop the synchronized typing over every visible windows.

The customized typing can also be stopped with keys defined in the list **`synchro-typing-stop-keys`**: ("M-x" "C-g") by default.  
Use **`customize-group synchro-typing`** to change its value.

***Why?***

Here is at least some use case where synchro typing may be useful:

  * Play the same command over multiple shells.
  * Edit multiple buffers at the same time (even in the same buffer like with [multiple-cursors](https://github.com/magnars/multiple-cursors.el) but in a less practical manner because each line should be visible in a separate window).
  * View multiple files side by side.
  * ...

***Unexpected behavior***

Be careful, keys which change the active window are repeated over each window. So at the end the cursor might not be where you expect.  
To Prevent this, one can add some keys in the **`synchro-typing-ignored-keys`** list.  
Initially this list contains the "C-x o" sequence, the windmove one ("`<S-arrows>`") and some kill ring sequences ("M-w" "C-w" "M-y").  
Likewise some other command may be wrongly played more than once. So customize those values to your needs with the **`customize-group synchro-typing`** command.

***Example***

![Screencast](https://bitbucket.org/pbrhocwp/synchro-typing-mode/raw/88e821a0c3eb278b279ddb640dcff18b7061a507/screencast.gif)

